package com.example.meisam1.myapplication.api;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;
import java.util.List;

public class DataSqlDs extends  SQLiteOpenHelper {

        String tableQuery = "" +
                " CREATE TABLE students ( " +
                " id INTEGER PRIMARY KEY AUTOINCREMENT , " +
                " name TEXT  ," +
                " family TEXT " +
                " )";

        public DataSqlDs(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(tableQuery);
        }

        public void insert(String name, String family) {
            String insertQuery = " INSERT INTO students (name,family) " +
                    " VALUES(  '" + name + "' , '" + family + "'   )";
            SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL(insertQuery);
            db.close();
        }

        public List<WeathearModel> getStudents() {
            List<WeathearModel> students = new ArrayList<>();

            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor = db.rawQuery("SELECT name,family FROM students ORDER BY id DESC", null);
            while (cursor.moveToNext()) {

                String name = cursor.getString(0);
                String family = cursor.getString(1);
                WeathearModel std = new WeathearModel(name, family);

                students.add(std);
            }

            return students;
        }


        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }
