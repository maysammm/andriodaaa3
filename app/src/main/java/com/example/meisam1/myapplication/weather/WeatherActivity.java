package com.example.meisam1.myapplication.weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meisam1.myapplication.R;
import com.example.meisam1.myapplication.weather.model.YhooModel;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.roger.catloadinglibrary.CatLoadingView;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener {

   ImageView image12,inn;
    Context mContext = this;
    EditText e1;
    Button ok;
    TextView tex2,tex1,tex3,tex4,tex5;
    TextView text08,text09;
    GridView list,list1;

    Spinner spinner;
    DataSql1 dbHandler1;

    CatLoadingView mView;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        mView = new CatLoadingView();


        dbHandler1=new DataSql1(this,"User2.db",null,1);

        inn=findViewById(R.id.inn);
        spinner=(Spinner)findViewById(R.id.spinner);
        text08=(TextView) findViewById(R.id.text08);
        list=(GridView) findViewById(R.id.list);
        e1=(EditText)findViewById(R.id.e1);
        ok=(Button)findViewById(R.id.ok);

        ok.setOnClickListener(this);

      spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
          @Override
          public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
              String sns= adapterView.getItemAtPosition(i).toString();
              getdatayahoo(sns);
          }

          @Override
          public void onNothingSelected(AdapterView<?> adapterView) {

          }
      });



      //  dialog = new ProgressDialog(mContext);
   //     dialog.setTitle("waiting");
     //   dialog.setCancelable(false);
    //    dialog.setMessage("Please wait for server response");


    }

    @Override
    public void onClick(View view) {
        mView.show(getSupportFragmentManager(), "logginnnnnn");
        String city=e1.getText().toString();
        if (!city.equals("")) {
            String date5 = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(new Date());
            String cap = city.substring(0, 1).toUpperCase() + city.substring(1);


          String date2= dbHandler1.getWether(cap,date5);
          String[] bd = date2.split(" ");
          text09.setText(bd[0]+bd[1]+bd[2]+bd[3]+bd[4]+bd[5]);
          text08.setText(cap+"\n"+date5+"\n"+date2);
           getdatayahoo(city);

//            dialog.onStart();
        }else{
            Toast.makeText(mContext, "null", Toast.LENGTH_SHORT).show();
            e1.setText("");
        }

    }




    void getdatayahoo(String city){
//         dialog.onStart();
         String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + city + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

         AsyncHttpClient client = new AsyncHttpClient();
         client.get(url, new TextHttpResponseHandler() {


             @Override
             public void onStart() {
                 super.onStart();
//                 dialog.show();
             }

             @Override
             public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                 Toast.makeText(mContext, "error" + throwable, Toast.LENGTH_SHORT).show();
             }

             @Override
             public void onSuccess(int statusCode, Header[] headers, String responseString) {
                 showdata(responseString);
             }


             @Override
             public void onFinish() {
                 super.onFinish();
              //   dialog.dismiss();
//                 mView.dismiss();
             }
         });
    }


    void showdata(String responseString){
        try {
    Gson Gson = new Gson();
    YhooModel yhooModel = Gson.fromJson(responseString, YhooModel.class);

    WeatherAdapter adapter = new WeatherAdapter(mContext, yhooModel.getQuery().getResults().getChannel().getItem().getForecast()) ;
    list.setAdapter(adapter);

            String city1 = yhooModel.getQuery().getResults().getChannel().getLocation().getCity();
            String temp10 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(0).getCode();
            String temp11 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(0).getDate();
            String temp21 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(0).getDay();
            String temp31 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(0).getHigh();
            String temp41 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(0).getLow();
            String temp51 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(0).getText();
            text08.setText("  city      "+city1+"\n"+"  date    "+temp11+"\n"+"  day      "+temp21+"\n"+"  hight    "+temp31+"\n"+"  low       "+temp41+"\n"+"  info weater    "+temp51);
            String code=yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(0).getCode();
            String url="http://l.yimg.com/a/i/us/we/52/"+ code +".gif";
             Picasso.get().load(url).into(inn);


    int size = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().size();

        for(int i=0 ; i<size ; i++) {

            String cit = yhooModel.getQuery().getResults().getChannel().getLocation().getCity();
            String tem = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getDate();

            String date = dbHandler1.getWether(cit, tem);
            //  text08.setText(cit+tem+date+"ss");
            if (!date.equals("")) {

            }else {
                String city = yhooModel.getQuery().getResults().getChannel().getLocation().getCity();
                String temp = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getCode();
                String temp1 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getDate();
                String temp2 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getDay();
                String temp3 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getHigh();
                String temp4 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getLow();
                String temp5 = yhooModel.getQuery().getResults().getChannel().getItem().getForecast().get(i).getText();
                dbHandler1.insert1ToDB(city, temp1, temp, temp2, temp3, temp4, temp5);

            }

  }





//tex1.setText(temp);
//tex2.setText(temp1);
//tex3.setText(temp2);
//tex4.setText(temp3);
//tex5.setText(temp4);


} catch (Exception e){
    Toast.makeText(mContext, "City not fond", Toast.LENGTH_SHORT).show();
}



    //  try{
       //   JSONObject allobject =new JSONObject(responseString);
    //       String query=allobject.getString("query");
    //       JSONObject queryobject=new JSONObject(query);
//
 //     }catch (Exception e){

 //     }










    }



}
