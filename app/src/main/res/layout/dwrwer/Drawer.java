package com.example.meisam1.myapplication.dwrwer;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;

import com.example.meisam1.myapplication.R;


public class Drawer extends AppCompatActivity {

    Button  taggle;
    DrawerLayout  derwer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer1);
        taggle=findViewById(R.id.taggle);
        derwer=findViewById(R.id.drawer);
        taggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!derwer.isDrawerOpen(Gravity.RIGHT)){
                    derwer.openDrawer(Gravity.RIGHT);
                }
            }

        });
    }
}
