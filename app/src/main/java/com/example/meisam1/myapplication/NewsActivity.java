package com.example.meisam1.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

public class NewsActivity extends AppCompatActivity implements View.OnClickListener {




    ImageView tag;
    DrawerLayout der;
    SimpleDraweeView circleImage;
    TextView text01,text02,text03;
    Context mContext = this;
    EditText e1;
    Button ok;
    ListView list;
    ImageView img,img1,img2,img3,img4;


    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.acivity_news);



        circleImage = (SimpleDraweeView) findViewById(R.id.circle);
        final Uri imageUri = Uri.parse("https://lh3.googleusercontent.com/-voUmhKJzNHc/VSJaPfSJ2pI/AAAAAAAABKw/-oFVzRZxI40/w140-h105-p/fresh_green_grass_bokeh-wallpaper-1024x768.jpg");
        circleImage.setImageURI(imageUri);


        text01=(TextView)findViewById(R.id.text01);
        text02=(TextView)findViewById(R.id.text02);
        text03=(TextView)findViewById(R.id.text03);
        img=(ImageView)findViewById(R.id.img);
        img1=(ImageView)findViewById(R.id.img1);
        img2=(ImageView)findViewById(R.id.img2);
        img3=(ImageView)findViewById(R.id.img3);
        img4=(ImageView)findViewById(R.id.img4);
        e1 = (EditText) findViewById(R.id.e1);
        ok = (Button) findViewById(R.id.ok);


        ok.setOnClickListener(this);
        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);
        text01.setOnClickListener(this);
        text02.setOnClickListener(this);
        text03.setOnClickListener(this);


        //  dialog = new ProgressDialog(mContext);
        //     dialog.setTitle("waiting");
        //   dialog.setCancelable(false);
        //    dialog.setMessage("Please wait for server response");
     img.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             String city1="bbc-news";
             getIntent(city1);
         }
     });

       tag=findViewById(R.id.tag);
       der=findViewById(R.id.der);
      tag.setOnClickListener(new View.OnClickListener() {
         @Override
           public void onClick(View v) {
               if(!der.isDrawerOpen(Gravity.RIGHT)){
                   der.openDrawer(Gravity.RIGHT);
             }
          }

     });
   }


    @Override
    public void onClick(View view) {
       if(view.getId()==R.id.ok) {
           try {
               String city = e1.getText().toString();
               //    getdatayahoo(city);
               if (!city.equals("")) {
                   String city1value = city;
                   if (city1value.length() != 0) {
                       getIntent(city1value);

                   } else {
                       Toast.makeText(mContext, "null", Toast.LENGTH_SHORT).show();
                       e1.setText("");
                   }


               }
           } catch (Exception e) {
               Toast.makeText(mContext, "ERR", Toast.LENGTH_SHORT).show();
           }
       }else if(view.getId()==R.id.img1){
           String city1="bbc-sport";
           getIntent(city1);
       }else if(view.getId()==R.id.img2){
           String city1="cnn";
           getIntent(city1);
       }else if(view.getId()==R.id.img3){
           String city1="cbc-news";
           getIntent(city1);
       }else if(view.getId()==R.id.img4){
           String city1="abc-news";
           getIntent(city1);
       }else if(view.getId()==R.id.text01){
           String city1="bbc-news";
           getIntent(city1);
       }else if(view.getId()==R.id.text02){
           String city1="cnn";
           getIntent(city1);
       }else if(view.getId()==R.id.text03){
           if(der.isDrawerOpen(Gravity.RIGHT)){
               der.closeDrawer(Gravity.RIGHT);
           }
       }

        }
      void  getIntent(String city1value){
            Intent Main2ActivityIntent = new Intent(NewsActivity.this, Main2Activity.class);
            Main2ActivityIntent.putExtra("city", city1value);
            startActivity(Main2ActivityIntent);
            finish();
        }

    }
