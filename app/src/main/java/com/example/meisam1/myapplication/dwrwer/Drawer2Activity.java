package com.example.meisam1.myapplication.dwrwer;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;

import com.example.meisam1.myapplication.R;

public class Drawer2Activity extends AppCompatActivity {


    Button taggle1;
    DrawerLayout derwer1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer2);

        taggle1=findViewById(R.id.taggle1);
        derwer1=findViewById(R.id.drawer1);
        taggle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!derwer1.isDrawerOpen(Gravity.RIGHT)){
                    derwer1.openDrawer(Gravity.RIGHT);
                }
            }

        });
    }
}
