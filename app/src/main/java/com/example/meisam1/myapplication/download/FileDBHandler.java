package com.example.meisam1.myapplication.download;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FileDBHandler extends SQLiteOpenHelper {

    String queryTBL = "" +
            " CREATE TABLE files (" +
            " id INTEGER PRIMARY KEY AUTOINCREMENT ," +
            " dl_date TEXT , " +
            " dl_origin  TEXT ," +
            " dl_destination  TEXT )" +
            "" ;
    public FileDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(queryTBL);
    }


    public void insert(String date , String origin , String destination){
        SQLiteDatabase db = this.getWritableDatabase() ;
        String insertQuery = " " +
                " INSERT INTO files(dl_date , dl_origin , dl_destination) " +
                " VALUES ('"+date+"' , '"+origin+"'  , '"+destination+"' ) " ;
        db.execSQL(insertQuery);
        db.close();
     }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
