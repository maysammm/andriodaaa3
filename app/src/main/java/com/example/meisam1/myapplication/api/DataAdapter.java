package com.example.meisam1.myapplication.api;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.meisam1.myapplication.R;

import java.util.List;
 public class DataAdapter extends BaseAdapter {

        Context mContext ;
        List<WeathearModel> students ;

        public DataAdapter(Context mContext, List<WeathearModel> students) {
            this.mContext = mContext;
            this.students = students;
        }

        @Override
        public int getCount() {
            return students.size();
        }

        @Override
        public Object getItem(int i) {
            return students.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v = LayoutInflater.from(mContext)
                    .inflate(R.layout.weather_database_item , viewGroup , false) ;

            TextView name = v.findViewById(R.id.name) ;
            TextView family = v.findViewById(R.id.family) ;

            name.setText(students.get(i).getName());
            family.setText(students.get(i).getFamily());
            return v;
        }
    }

