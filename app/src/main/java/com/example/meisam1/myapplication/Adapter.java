package com.example.meisam1.myapplication;

import android.content.Context;
import android.net.Uri;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.meisam1.myapplication.model.Article;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter extends BaseAdapter {
    Context mContext;
    List<Article> forecasts;

    public Adapter(Context mContext, List<Article> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int position) {
        return forecasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View row = LayoutInflater.from(mContext).inflate(R.layout.news_item_list,viewGroup,false);


        TextView day=(TextView)row.findViewById(R.id.day1);
        TextView hig=(TextView)row.findViewById(R.id.high1);
        TextView low=(TextView)row.findViewById(R.id.low1);
        TextView text=(TextView)row.findViewById(R.id.text1);
        TextView text1=(TextView)row.findViewById(R.id.text2);
        ImageView ica=(ImageView) row.findViewById(R.id.ica);


           day.setText(forecasts.get(position).getAuthor());
           hig.setText(forecasts.get(position).getTitle());
           low.setText(forecasts.get(position).getPublishedAt());
           text.setText(forecasts.get(position).getDescription());
           String urln=forecasts.get(position).getUrl();
            text1.setText(urln);
            Linkify.addLinks(text1, Linkify.WEB_URLS);


        String url = forecasts.get(position).getUrlToImage();
        Picasso.get().load(url).into(ica);

        return row;
    }
}
