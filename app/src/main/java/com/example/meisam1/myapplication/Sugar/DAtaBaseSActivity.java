package com.example.meisam1.myapplication.Sugar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.meisam1.myapplication.R;

public class DAtaBaseSActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base_s);

        DataBaseSModel book = new DataBaseSModel( "Title here", "2nd edition");
        book.save();
    }
}
