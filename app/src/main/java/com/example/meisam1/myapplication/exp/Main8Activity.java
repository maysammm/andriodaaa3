package com.example.meisam1.myapplication.exp;

import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.meisam1.myapplication.ItemAdapter;
import com.example.meisam1.myapplication.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

public class Main8Activity extends AppCompatActivity {

    SimpleDraweeView circleImage;
    Context mContext=this;
    ListView  text32;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main8);



        circleImage = (SimpleDraweeView) findViewById(R.id.circle);
        final Uri imageUri = Uri.parse("https://lh3.googleusercontent.com/-voUmhKJzNHc/VSJaPfSJ2pI/AAAAAAAABKw/-oFVzRZxI40/w140-h105-p/fresh_green_grass_bokeh-wallpaper-1024x768.jpg");
        circleImage.setImageURI(imageUri);

        text32=findViewById(R.id.text32);
        String[] items1 = new String[] { "Chai Latte", "Green Tea", "Black Tea","yahoo","aol","google","yahoo" };
        ItemAdapter1 adapter= new ItemAdapter1(mContext,items1);
        text32.setAdapter(adapter);


    }
}
