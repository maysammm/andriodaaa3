package com.example.meisam1.myapplication.TextView;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViews extends TextView {
    public TextViews(Context context) {

        super(context);

    }

    public TextViews(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface ir=Typeface.createFromAsset(context.getAssets(),"ir_sans.ttf");
        this.setTypeface(ir);
    }


}
