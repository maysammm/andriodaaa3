package com.example.meisam1.myapplication.dwrwer;

import com.orm.SugarRecord;

public class DataModel  extends SugarRecord<DataModel> {

    public String FullName;
    public String Phonenumber;
    public String Email;
    public String UeraName;
    public String PassWord;

    public DataModel(String fullName, String phonenumber, String email, String ueraName, String passWord) {
        FullName = fullName;
        Phonenumber = phonenumber;
        Email = email;
        UeraName = ueraName;
        PassWord = passWord;
    }

    public DataModel() {
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getPhonenumber() {
        return Phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        Phonenumber = phonenumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUeraName() {
        return UeraName;
    }

    public void setUeraName(String ueraName) {
        UeraName = ueraName;
    }

    public String getPassWord() {
        return PassWord;
    }

    public void setPassWord(String passWord) {
        PassWord = passWord;
    }
}
