package com.example.meisam1.myapplication.exp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.meisam1.myapplication.R;

import es.dmoral.toasty.Toasty;

import static cz.msebera.android.httpclient.extras.PRNGFixes.apply;

public class Main7Activity extends AppCompatActivity implements View.OnClickListener {

    WebView web;
    EditText et;
   Context mContext=this;
    Button ma,sa;
   ProgressDialog pro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);
        ma=findViewById(R.id.ma);
        sa=findViewById(R.id.sa);
        ma.setOnClickListener(this);
        sa.setOnClickListener(this);
        web=findViewById(R.id.web);
        et=findViewById(R.id.et);
        pro=new ProgressDialog(mContext);
        pro.setTitle("درحال لود کردن");
        pro.setMessage("لطفا صبرکنید");

        Typeface ir =Typeface.createFromAsset(getAssets(),"ir_sans.ttf");
        ma.setTypeface(ir);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0,1,1,"email").setIcon(R.drawable.ic_launcher);
        menu.add(0,10,1,"page");
       getMenuInflater().inflate(R.menu.menu,menu);




        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==1){
            Toast.makeText(this, "email", Toast.LENGTH_SHORT).show();
        }
        if(item.getItemId()==10){
            Toast.makeText(this, "page", Toast.LENGTH_SHORT).show();
        }
        if (item.getItemId()==R.id.about){
            Toast.makeText(this, "about", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
      if(v.getId()==R.id.ma){
          alrteyn();

      }
      if(v.getId()==R.id.sa){
          web.getSettings().setJavaScriptEnabled(true);
          web.setWebViewClient(new Main7Activit());
       String url=et.getText().toString();
         if(!url.startsWith("http://")){
             url="http://"+url;
         }
          web.loadUrl(url);
      }

    }
    void  alrteok(){
        AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle("Alert Dialog");
        alertDialog.setMessage("Welcome to dear user.");
        alertDialog.setIcon(R.drawable.ic_launcher);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialog.show();

    }

void alrteyn(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mContext);

        // set title
        alertDialogBuilder.setTitle("Your Title");

        // set dialog message
        alertDialogBuilder
                .setMessage("Click yes to exit!")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, close
                        // current activity
                        Main7Activity.this.finish();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
    class Main7Activit extends  WebViewClient{
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
             pro.show();
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            pro.dismiss();
            super.onPageFinished(view, url);
        }
    }
    }

