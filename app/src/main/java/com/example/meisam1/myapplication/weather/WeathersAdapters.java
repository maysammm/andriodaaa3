package com.example.meisam1.myapplication.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.meisam1.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class WeathersAdapters extends BaseAdapter {
    Context mContext;
    List<WeathersModels> weathersModelsList;

    public WeathersAdapters(Context mContext, List<WeathersModels> weathersModelsList) {
        this.mContext = mContext;
        this.weathersModelsList = weathersModelsList;
    }

    @Override
    public int getCount() {
        return weathersModelsList.size();
    }

    @Override
    public Object getItem(int i) {
        return weathersModelsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row= LayoutInflater.from(mContext).inflate(R.layout.weathers_database_item,viewGroup,false);

        TextView city1 = row.findViewById(R.id.city1) ;
        TextView code1 = row.findViewById(R.id.code1) ;
        TextView date1 = row.findViewById(R.id.date1) ;
        TextView day1 = row.findViewById(R.id.day1) ;
        TextView high1 = row.findViewById(R.id.high1) ;
        TextView low1  = row.findViewById(R.id.low1 ) ;
        TextView text1 = row.findViewById(R.id.text1 ) ;
        ImageView img = row.findViewById(R.id.img ) ;


        city1.setText(weathersModelsList.get(i).getCity());
        date1.setText(weathersModelsList.get(i).getDate());
        day1.setText(weathersModelsList.get(i).getDay());
        high1.setText(weathersModelsList.get(i).getLow());
        low1.setText(weathersModelsList.get(i).getLow());
        text1.setText(weathersModelsList.get(i).getText());

        String code = weathersModelsList.get(i).getCode();
        String url = "http://l.yimg.com/a/i/us/we/52/" + code + ".gif";
        Picasso.get().load(url).into(img);

        return row;
    }
}
