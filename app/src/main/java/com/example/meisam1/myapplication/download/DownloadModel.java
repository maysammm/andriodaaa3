package com.example.meisam1.myapplication.download;

import com.orm.SugarRecord;

public class DownloadModel extends SugarRecord<DownloadModel> {
    public String url ;
    public String date ;
    public String downloadedAddress ;

    public DownloadModel(String url, String date, String downloadedAddress) {
        super();
        this.url = url;
        this.date = date;
        this.downloadedAddress = downloadedAddress;
    }

    public DownloadModel() {
        super();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDownloadedAddress() {
        return downloadedAddress;
    }

    public void setDownloadedAddress(String downloadedAddress) {
        this.downloadedAddress = downloadedAddress;
    }
}
