package com.example.meisam1.myapplication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meisam1.myapplication.TextView.Main11Activity;
import com.example.meisam1.myapplication.exp.ItemAdapter1;
import com.example.meisam1.myapplication.exp.Main7Activity;
import com.example.meisam1.myapplication.imdb.ImDbActivity;
import com.example.meisam1.myapplication.weather.WeatherActivity;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    SimpleDraweeView circleImage;
    Context mContext = this;
    ListView list01,text21;
    TextView text001,Tm1;
    TextView text002;
    TextView text003;
    ImageView menu;
    DrawerLayout  draw;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);

        circleImage = (SimpleDraweeView) findViewById(R.id.circle);
        final Uri imageUri = Uri.parse("https://lh3.googleusercontent.com/-voUmhKJzNHc/VSJaPfSJ2pI/AAAAAAAABKw/-oFVzRZxI40/w140-h105-p/fresh_green_grass_bokeh-wallpaper-1024x768.jpg");
        circleImage.setImageURI(imageUri);

        text21=findViewById(R.id.text21);
        String[] items1 = new String[] { "  Imdb   ", "  NewsPaper  ", "   Weather  ","  Quit  " };
        String[] items = new String[] { "  film  ", "  news  ", "  yahoo  "," exit " };
        int[] items2={R.drawable.c1,R.drawable.c2,R.drawable.c3,R.drawable.c5};

        ItemAdapter adapter= new ItemAdapter(mContext,items1,items,items2);
        text21.setAdapter(adapter);


        text21.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String sh= parent.getItemAtPosition(position).toString();
                Toasty.success(mContext, sh+"", Toast.LENGTH_SHORT, true).show();
                   if(sh.equals("  NewsPaper  ")){
                       Intent Main2ActivityIntent = new Intent(mContext, NewsActivity.class);
                       startActivity(Main2ActivityIntent);
                       finish();
                       Toasty.success(mContext, "error", Toast.LENGTH_SHORT, true).show();
                       text21.setBackgroundColor(Color.rgb(173, 211, 224));
                   }else if(sh.equals("   Weather  ")){
                       Intent Main2ActivityIntent = new Intent(mContext, WeatherActivity.class);
                       startActivity(Main2ActivityIntent);
                       finish();
                   }else if(sh.equals("  Imdb   ")){
                       Intent Main2ActivityIntent = new Intent(mContext, ImDbActivity.class);
                       startActivity(Main2ActivityIntent);
                       finish();
                   }else if (sh.equals("  Quit  ")){
                        conform();

                   }

            }
        });



       Tm1=findViewById(R.id.Tm1);




        menu=findViewById(R.id.menu);
        draw=findViewById(R.id.draw);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!draw.isDrawerOpen(Gravity.RIGHT)){
                    draw.openDrawer(Gravity.RIGHT);

                }
            }

        });
        String recres=" سلام "+getIntent().getStringExtra( "name" )+"  عزیز   خوش امدید";
        Tm1.setText(recres);



    }
    @Override
    public void onBackPressed() {
        Toast.makeText(mContext, "Dont back page", Toast.LENGTH_SHORT).show();
        moveTaskToBack(true);
    }
    @Override
    public void onClick(View v) {

    }
    public void onDestroy(){
        super.onDestroy();
    }
    void conform(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mContext);
        // set title
        alertDialogBuilder.setTitle("ایا می خواهید از برنامه خارج شوید");
        // set dialog message
        alertDialogBuilder
                .setMessage("دکمه ok را فشار دهید")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(mContext, "CANCEL button click ", Toast.LENGTH_SHORT).show();

                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }
    void  vv(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure you want to delete this entry?");

        builder.setPositiveButton("Yes, please", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //perform any action
                Toast.makeText(getApplicationContext(), "Yes clicked", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //perform any action
                Toast.makeText(getApplicationContext(), "No clicked", Toast.LENGTH_SHORT).show();
            }
        });
        //creating alert dialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
