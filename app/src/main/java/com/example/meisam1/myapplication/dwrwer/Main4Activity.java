package com.example.meisam1.myapplication.dwrwer;

import android.content.Context;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.meisam1.myapplication.R;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

public class Main4Activity extends AppCompatActivity implements View.OnClickListener {

    Context mContext = this;

    EditText  TSingUp11,TSingUp12,TSingUp13,TSingUp14,TSingUp15;
    Button  BSingUp11;
    SimpleDraweeView circle1Image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main4);

        TSingUp11=(EditText) findViewById(R.id.TSingUp11);
        TSingUp12=(EditText) findViewById(R.id.TSingUp21);
        TSingUp13=(EditText) findViewById(R.id.TSingUp31);
        TSingUp14=(EditText) findViewById(R.id.TSingUp41);
        TSingUp15=(EditText) findViewById(R.id.TSingUp51);
        BSingUp11=(Button) findViewById(R.id.BSingUp11);

        BSingUp11.setOnClickListener(this);



        circle1Image = (SimpleDraweeView) findViewById(R.id.circle1);
        Uri imageUri = Uri.parse("https://lh3.googleusercontent.com/-voUmhKJzNHc/VSJaPfSJ2pI/AAAAAAAABKw/-oFVzRZxI40/w140-h105-p/fresh_green_grass_bokeh-wallpaper-1024x768.jpg");
        circle1Image.setImageURI(imageUri);

    }

    @Override
    public void onClick(View v) {


          DataModel book = new DataModel();
          book.setFullName(TSingUp11.getText().toString());
          book.setEmail(TSingUp12.getText().toString());
          book.setPhonenumber(TSingUp13.getText().toString());
          book.setUeraName(TSingUp14.getText().toString());
          book.setPassWord(TSingUp15.getText().toString());
          book.save();
        Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();
    }
}
