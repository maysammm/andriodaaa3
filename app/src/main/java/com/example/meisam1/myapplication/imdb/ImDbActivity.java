package com.example.meisam1.myapplication.imdb;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meisam1.myapplication.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;


public class ImDbActivity extends AppCompatActivity implements View.OnClickListener {


            int you=0;
            Context mContext=this;
            EditText word;
            TextView title,Year,Genre,Writer,Country;
            TextView director;
            ImageView poster;
            DataSqlDb db;

            ProgressDialog dialog;

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_imdb);


                bind();
                dialog = new ProgressDialog(mContext);
                dialog.setTitle("waiting");
                dialog.setCancelable(false);
                dialog.setMessage("Please wait for server response");

                db = new DataSqlDb(this,"User.db",null,1);




            }

            void bind() {
                word = (EditText) findViewById(R.id.word);
                title = (TextView) findViewById(R.id.title);
                director = (TextView) findViewById(R.id.director);
                poster = (ImageView) findViewById(R.id.poster);
                Genre = (TextView) findViewById(R.id.Genre);
                Year = (TextView) findViewById(R.id.Year);
                Writer = (TextView) findViewById(R.id.Writer);
                Country = (TextView) findViewById(R.id.Country);
                findViewById(R.id.search).setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.search) {
                 //   http://meisam-moh.ir/vo.php?job=16

if (you==0){
    String ma="http://meisam-moh.ir/vo.php";
    search(ma);
}
                    String ma="http://meisam-moh.ir/vo.php";
                    String word1 = word.getText().toString();



    try {

        String t1=word.getText().toString();
       String sn= db.getWether(t1);
       if (sn.equals("")) {
           Toast.makeText(mContext, "input", Toast.LENGTH_SHORT).show();
        search(t1);
    } else {

           String[] bd = sn.split("   ");
           title.setText(bd[1]);
           director.setText(bd[2]);
           Picasso.get().load(bd[3]).into(poster);
           Year.setText(bd[4]);
           Genre.setText(bd[5]);
           Writer.setText(bd[6]);
           Country.setText(bd[7]);
           Toast.makeText(mContext, "databast", Toast.LENGTH_SHORT).show();
   }
}catch (Exception e){
    Toast.makeText(mContext, "ee"+e, Toast.LENGTH_SHORT).show();

}
   }
       }

          void search(String word) {
                dialog.onStart();
              String url = "http://www.omdbapi.com/?t=" + word + "&apikey=70ad462a";
                AsyncHttpClient client = new AsyncHttpClient();
                client.get(url, new TextHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        dialog.show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toast.makeText(mContext, "Error : "+ throwable, Toast.LENGTH_SHORT).show();



                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        // PublicMethods.showToast(mContext, responseString);
                        parseServerResponse(responseString);

                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        dialog.dismiss();
                    }
                });
            }


            void parseServerResponse(String serverResponse) {

                try {

                    JSONObject allObject = new JSONObject(serverResponse);

                    String titleValue = allObject.getString("Title");
                    String diretorValue = allObject.getString("Director");
                    String posterValue = allObject.getString("Poster");
                    String YearValue = allObject.getString("Year");
                    String GenreValue = allObject.getString("Genre");
                    String WriterValue = allObject.getString("Writer");
                    String CountryValue = allObject.getString("Country");



                    title.setText(titleValue);
                    director.setText(diretorValue);
                    Picasso.get().load(posterValue).into(poster);
                    Year.setText(YearValue);
                    Genre.setText(GenreValue);
                    Writer.setText(WriterValue);
                    Country.setText(CountryValue);

                    db.insert1ToDB(titleValue,diretorValue,posterValue,YearValue,WriterValue,CountryValue,GenreValue);





                } catch (Exception e) {

                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();

                }

            }


        }
