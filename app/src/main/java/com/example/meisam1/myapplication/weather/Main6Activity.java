package com.example.meisam1.myapplication.weather;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meisam1.myapplication.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.StrictMath.log;

public class Main6Activity extends AppCompatActivity implements View.OnClickListener {



    List<Integer> img;
    ImageView ja;
    TextView ta;
    Context mContext = this;
    Button ok,checkIntrnetButton;
    DataSql2 dbHandler;
    GridView  list2;
    Integer i=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);
        ta=findViewById(R.id.ta);
        list2=findViewById(R.id.list2);
        ja=findViewById(R.id.ja);
        ok=findViewById(R.id.ok);
         dbHandler=new DataSql2(this,"User2.db",null,1);
         ok.setOnClickListener(this);

         img=new ArrayList<>();
         img.add(R.drawable.a1);
         img.add(R.drawable.a22);
         img.add(R.drawable.a23);
         img.add(R.drawable.a24);

          setimg();
        checkIntrnetButton = (Button)findViewById(R.id.checkInternetButton);
        checkIntrnetButton.setOnClickListener(new View.OnClickListener() {
            private boolean sh;

            @Override
            public void onClick(View v) {
                checkConnection();
                isNetworkAvailable();
                Toast.makeText(mContext, isNetworkAvailable()+"", Toast.LENGTH_SHORT).show();
            }
        });

        }

    public static boolean isNetworkAvailable () {

        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e){
            e.printStackTrace();
        } catch (InterruptedException e){
            e.printStackTrace();
        }
        return false;
    }


    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
    public void checkConnection(){
        if(isOnline()){
            Toast.makeText(Main6Activity.this, "You are connected to Internet", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(Main6Activity.this, "You are not connected to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    void  setimg(){

     new Handler().postDelayed(new Runnable() {
         @Override
         public void run() {


                 Picasso.get().load(img.get(i)).into(ja);
               if(i<img.size()-1) {
                   setimg();
                   i++;
               }else{
                   setimg();
                   i=0;
               }
         }
     },2000);

 }
    @Override
    public void onClick(View view) {
        ta.setText(dbHandler.getWether("Tehran","14 Jun 2018"));
        List<WeathersModels> weathersModels = dbHandler.getStudents1();
        String n=dbHandler.getStudents1().toString();
        WeathersAdapters adapter = new WeathersAdapters(mContext , weathersModels) ;
        list2.setAdapter(adapter);


    }
}
