package com.example.meisam1.myapplication.api;

import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.meisam1.myapplication.R;

import java.util.Date;
import java.util.Locale;

public class Main5Activity extends AppCompatActivity {


    TextView  t1;
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);


        t1=findViewById(R.id.t1);
        String date = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(new Date());
        t1.setText(date);

        String str = "tehran";
        String cap = str.substring(0, 1).toUpperCase() + str.substring(1);
        t1.setText(cap);
    }
}
