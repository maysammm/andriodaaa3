package com.example.meisam1.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meisam1.myapplication.model.NewsPaper;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class Main2Activity extends AppCompatActivity {


    Context mContext = this;

    TextView result1;
    ListView list1;
    ImageView ima;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        list1=(ListView) findViewById(R.id.list1);
        ima=(ImageView) findViewById(R.id.ima);
        result1=(TextView) findViewById(R.id.result1);
        ima.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               String name="ali";
                getIntent(name);
            }
        });

        String recres=getIntent().getStringExtra( "city" );
        result1.setText( recres );
        getdatayahoo(recres);



    }

    void getdatayahoo(final String city){
        try{
            //         dialog.onStart();
            String url = "https://newsapi.org/v2/top-headlines?sources="+ city +"&apiKey=aac65d9a911d4e14a5dfe61193747059";
            result1.setText(city);
            AsyncHttpClient client = new AsyncHttpClient();
            client.get(url, new TextHttpResponseHandler() {


                @Override
                public void onStart() {
                    super.onStart();
//                 dialog.show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Toast.makeText(mContext, "Error" + throwable, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    showdata(responseString,city);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    //   dialog.dismiss();

                }
            });

        }catch(Exception e ){
            Toast.makeText(mContext, "ERRorhhtp", Toast.LENGTH_SHORT).show();
        }
    }
    void showdata(String responseString,String city){

        try {
            Gson Gson = new Gson();
            NewsPaper newsPaper = Gson.fromJson(responseString, NewsPaper.class);
            String temp = newsPaper.getStatus();

            Adapter adapter = new Adapter(mContext,
                    newsPaper.getArticles()) ;

            list1.setAdapter(adapter);

            result1.setText(city+"-"+temp);

        } catch (Exception e){
            Toast.makeText(mContext, "City not fond", Toast.LENGTH_SHORT).show();
        }



        //  try{
        //   JSONObject allobject =new JSONObject(responseString);
        //       String query=allobject.getString("query");
        //       JSONObject queryobject=new JSONObject(query);
//
        //     }catch (Exception e){

        //     }










    }

    void  getIntent(String city1value){
        Intent Main2ActivityIntent = new Intent(Main2Activity.this, NewsActivity.class);
        Main2ActivityIntent.putExtra("city", city1value);
        startActivity(Main2ActivityIntent);
        finish();
    }

}

