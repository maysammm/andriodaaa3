package com.example.meisam1.myapplication.Sugar;

import com.orm.SugarRecord;

public class DataBaseSModel extends SugarRecord<DataBaseSModel> {
        String  name;
        String family;

    public DataBaseSModel(String name, String family) {
        this.name = name;
        this.family = family;
    }

    public DataBaseSModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }
}
