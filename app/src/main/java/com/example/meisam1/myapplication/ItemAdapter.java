package com.example.meisam1.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ItemAdapter extends BaseAdapter {

    Context mContext;
    String[] items1;
    String[] items;
    int[]     items2;

    public ItemAdapter(Context mContext, String[] items1, String[] items, int[] items2) {
        this.mContext = mContext;
        this.items1 = items1;
        this.items = items;
        this.items2 = items2;
    }

    @Override
    public int getCount() {
        return items1.length;
    }

    @Override
    public Object getItem(int position) {
        return items1[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View row= LayoutInflater.from(mContext).inflate(R.layout.simple_item,parent,false);

        TextView text1011=(TextView)row.findViewById(R.id.text1011);
        TextView text1012=(TextView)row.findViewById(R.id.text1012);
        ImageView ima11=(ImageView)row.findViewById(R.id.ima11);
       // ima11.setImageResource(items2[position]);
        int url = items2[position];
        Picasso.get().load(url).into(ima11);
        text1012.setText(items[position]);
        text1011.setText(items1[position]);

        return row;
    }
}
