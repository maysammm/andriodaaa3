package com.example.meisam1.myapplication.exp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.meisam1.myapplication.R;

public class ItemAdapter1 extends BaseAdapter {

    Context mContext;
    String[] items1;

    public ItemAdapter1(Context mContext, String[] items1) {
        this.mContext = mContext;
        this.items1 = items1;
    }

    @Override
    public int getCount() {
        return items1.length;
    }

    @Override
    public Object getItem(int position) {
        return items1[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View row= LayoutInflater.from(mContext).inflate(R.layout.simple_item,parent,false);

        TextView text1011=(TextView)row.findViewById(R.id.text1011);
        text1011.setText(items1[position]);

        return row;
    }
}
