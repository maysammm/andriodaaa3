package com.example.meisam1.myapplication.download;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.meisam1.myapplication.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class DownloaderActivity extends AppCompatActivity {

    Context mContext=this;
    EditText url;
    EditText age;
    ProgressBar downloadPercent;
    Button download;
    TextView percent;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloader);
        bind();

        for (int count = 0; count < 10; count++) {
            if (count == 3)
                continue;
        }

    }

    void bind() {
        age = findViewById(R.id.age);
        url = findViewById(R.id.age);
        result = findViewById(R.id.result);
        downloadPercent = findViewById(R.id.downloadPercent);
        download = findViewById(R.id.download);
        percent = findViewById(R.id.percent);
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    download(url.getText().toString());
            }
        });
    }

    private void download(final String urlValue) {
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(urlValue, new FileAsyncHttpResponseHandler(mContext) {

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                int percentage = (int) (bytesWritten * 100.0 / totalSize + 0.5);
                downloadPercent.setProgress(percentage);
                percent.setText(percentage + "%");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Toast.makeText(mContext, "error in downloading file", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                String fileAddress =
                        file.getAbsolutePath();

                File newFile = new File("/sdcard/myfiles/video.mp4");
                file.renameTo(newFile);

//                VideoView test = new VideoView(mContext) ;
//                test.setVideoURI(Uri.parse("/sdcard/myfiles/video.mp4"));
//                test.start();

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                String currentDateandTime = sdf.format(new Date());
           //     DownloadModel dl = new DownloadModel();
           //     dl.setDate(currentDateandTime);
           //     dl.setUrl(urlValue);
           //     dl.setDownloadedAddress(fileAddress);
           //     dl.save();
           //     url.setText("");
                Toast.makeText(mContext, "file has been downloaded", Toast.LENGTH_SHORT).show();

                List<DownloadModel> downloads =
                        DownloadModel.listAll(DownloadModel.class);


                FileDBHandler db = new FileDBHandler(
                        mContext, "new_db.db", null, 1
                );
                db.insert(currentDateandTime, urlValue, fileAddress);


            }
        });

    }
}
