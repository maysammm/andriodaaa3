package com.example.meisam1.myapplication.api;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.meisam1.myapplication.R;

import java.util.List;

public class DataActivity extends AppCompatActivity {

    Context mContext=this;
    EditText name, family;
    Button save,show;
    DataSqlDs db;
    ListView list ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);



        db = new DataSqlDs(this,
                "sematec.db"
                , null, 1);

        name = findViewById(R.id.name);
        family = findViewById(R.id.family);
        save = findViewById(R.id.save);
        show = findViewById(R.id.show);
        list = findViewById(R.id.list);

        show();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.insert(name.getText().toString()
                        , family.getText().toString());
                Toast.makeText(mContext, "new user has been added", Toast.LENGTH_SHORT).show();
                name.setText("");
                family.setText("");
                show();
            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show() ;
            }
        });
    }


    void show(){
        List<WeathearModel> students = db.getStudents() ;
        DataAdapter adapter = new DataAdapter(
                mContext , students
        ) ;
        list.setAdapter(adapter);
    }
}