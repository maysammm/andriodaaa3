package com.example.meisam1.myapplication.download;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.meisam1.myapplication.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;

import cz.msebera.android.httpclient.Header;

public class DownloadActivity extends AppCompatActivity {

    Context mContext;
    EditText  url;
    Button    OK1;
    ProgressBar pro;
    TextView   t101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);


        url=findViewById(R.id.url);
        OK1=findViewById(R.id.OK1);
        pro=findViewById(R.id.pro);
        t101=findViewById(R.id.t101);
        OK1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                download(url.getText().toString());
            }
        });


    }
  public   void download(String url){

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(mContext) {
            @Override
            public void onStart() {
                super.onStart();
//                 dialog.show();
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                int pro1=(int)(bytesWritten*100.0/totalSize+0.5);
                pro.setProgress(pro1);
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {





            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {

                String address=file.getAbsolutePath();

            }




            @Override
            public void onFinish() {
                super.onFinish();
                //   dialog.dismiss();

            }

        });

}
}